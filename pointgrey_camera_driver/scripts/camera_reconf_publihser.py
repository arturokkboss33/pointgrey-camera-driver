#!/usr/bin/python

import rospy
from std_msgs.msg import String

### REF: Valid input options ###
#"fr":"frame_rate"
#"wb":"white_balance_blue"
#"wr":"white_balance_red"
#"aw":"auto_white_balance"
#"png":"png_level"
#"jp":"jpeg_quality"


### Publisher method ###
def cam_param_publisher():
	pub = rospy.Publisher('/camera/dynamic_params_reconf', String, queue_size = 2);
	rospy.init_node('camera_reconf_publisher', anonymous=True)
	rate = rospy.Rate(5)
	flag_end_node = False

	while not rospy.is_shutdown() and not flag_end_node:
		user_args = raw_input("==> ")
		rospy.loginfo(user_args)

		args_list = user_args.split(" ");
		if args_list[0] != "q":
			pub.publish(user_args)
		else:
			flag_end_node = True
		rate.sleep()

### MAIN METHOD ###
if __name__ == '__main__':
	try:
		cam_param_publisher()
	except rospy.ROSInterruptException:
		pass