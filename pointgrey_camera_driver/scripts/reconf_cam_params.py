import subprocess
import os

param_dict = {"fr":"frame_rate", "wb":"white_balance_blue", "wr":"white_balance_red", "aw":"auto_white_balance", "png":"png_level", "jp":"jpeg_quality"}
str_cam_param_name = "camera_param:="
str_cam_param_val = "camera_param_value:="
str_compression_flag = "compression_flag:="
str_compression_val = "compression_value:="
str_param_group = "param_group:="

if __name__ == "__main__":
	op = ""
	cmd = []
	while op != "q":
		cmd = []
		user_args = raw_input("==> ")
		args_list = user_args.split(" ")
		#print(type(args_list))
		#print(args_list)
		op = args_list[0]
		if op != "q":
			if args_list[0] == "def":
				param_group = str_param_group + "true"
				cmd = ["roslaunch", "pointgrey_camera_driver", "cameras_param_reconfigure.launch", "camera_default:=true", param_group]
			elif args_list[0] == "fr" or  args_list[0] == "wb" or args_list[0] == "wr" or args_list[0] == "aw":
				#print("camera")
				param_name = str_cam_param_name + param_dict[args_list[0]]
				param_val = str_cam_param_val + args_list[1]
				param_group = str_param_group + "true"
				cmd = ["roslaunch", "pointgrey_camera_driver", "cameras_param_reconfigure.launch", "camera_default:=false", param_group, param_name, param_val]
			elif args_list[0] == "png" or args_list[0] == "jp":
				#print("compression")
				comp_flag = "0" if args_list[0] == "png" else "1"
				param_name = str_compression_flag + comp_flag
				param_val = str_compression_val + args_list[1]
				param_group = str_param_group + "false"
				cmd = ["roslaunch", "pointgrey_camera_driver", "cameras_param_reconfigure.launch", param_group, param_name, param_val]
			else:
				print("no option available")
			if cmd:	
				#print(" ".join(cmd))
				FNULL = open(os.devnull, 'w')
				retcode = subprocess.call(cmd, stdout=FNULL, stderr=subprocess.STDOUT)
				
			
