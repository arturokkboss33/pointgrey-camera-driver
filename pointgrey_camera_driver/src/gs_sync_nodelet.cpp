//syncing node h
#include <nodelet/nodelet.h>
#include <pluginlib/class_list_macros.h>
#include "ros/ros.h"
#include <image_transport/image_transport.h> // ROS library that allows sending compressed images
#include <camera_info_manager/camera_info_manager.h> // ROS library that publishes CameraInfo topics
#include <sensor_msgs/CameraInfo.h> // ROS message header for CameraInfo
#include <opencv2/highgui/highgui.hpp>
#include <cv_bridge/cv_bridge.h>
#include <iostream>
#include <vector>
#include <string>


#include <wfov_camera_msgs/jir_capture_msg.h>
#include <message_filters/subscriber.h>
#include <message_filters/time_synchronizer.h>
#include <message_filters/sync_policies/exact_time.h>
#include <message_filters/sync_policies/approximate_time.h>
#include <sensor_msgs/Image.h>
#include <sensor_msgs/CameraInfo.h>
#include <unistd.h>






using namespace sensor_msgs;
using namespace message_filters;
using namespace std;

namespace pointgrey_camera_driver
{

    class PointGreyGSCamerasSyncNodelet : public nodelet::Nodelet
    {
        public:
            PointGreyGSCamerasSyncNodelet () {};
            ~PointGreyGSCamerasSyncNodelet () {
                delete sync;
                delete image_sub_left;
                delete image_sub_center;
                delete image_sub_right;

                delete it;

            };
            
            void imageCallback(const ImageConstPtr& imageL, const ImageConstPtr& imageR, const ImageConstPtr& imageC)
            {
            ImagePtr StereoLeft= cv_bridge::CvImage(std_msgs::Header(), "bgr8", cv_bridge::toCvShare(imageL, "bgr8")->image).toImageMsg();;
            ImagePtr StereoCenter= cv_bridge::CvImage(std_msgs::Header(), "bgr8", cv_bridge::toCvShare(imageC, "bgr8")->image).toImageMsg();;
            ImagePtr StereoRight= cv_bridge::CvImage(std_msgs::Header(), "bgr8", cv_bridge::toCvShare(imageR, "bgr8")->image).toImageMsg();;
NODELET_ERROR("Sync nodelet!");
                NODELET_WARN("Sync nodelet!");
                NODELET_ERROR("Sync nodelet!");
                NODELET_ERROR("Sync nodelet!");
                NODELET_ERROR("Sync nodelet!");

            StereoLeft->header=StereoRight->header;
            StereoCenter->header=StereoRight->header;
            left_pub.publish(StereoLeft);
            right_pub.publish(StereoRight);
            center_pub.publish(StereoCenter);
            }
            
            
            
            void onInit(){
                ros::NodeHandle &nh = getMTNodeHandle();
                ros::NodeHandle &pnh = getMTPrivateNodeHandle();
                
                /*pnh.param<std::string>("img_topic_left", img_topic_left,"");
                pnh.param<std::string>("img_topic_right", img_topic_right,"");
                pnh.param<std::string>("img_topic_center", img_topic_center,"");*/

                NODELET_ERROR("Sync nodelet!");
                NODELET_ERROR("Sync nodelet!");
                NODELET_ERROR("Sync nodelet!");
                NODELET_ERROR("Sync nodelet!");
                NODELET_ERROR("Sync nodelet!");

                /*
                pnh.param<std::string>("frame_id_left", frame_id_left, "camera");
                pnh.param<std::string>("frame_id_right", frame_id_right, "camera");
                pnh.param<std::string>("frame_id_center", frame_id_center, "camera");
*/
                

                it=new image_transport::ImageTransport (nh);
                left_pub=it->advertise("stereo_camera/left/image", 1);
                right_pub=it->advertise("stereo_camera/right/image", 1);
                center_pub=it->advertise("stereo_camera/center/image", 1);



                image_sub_left=new message_filters::Subscriber<Image> (nh, "/left/image_color", 1);
                image_sub_right=new message_filters::Subscriber<Image> (nh, "/right/image_color", 1);
                image_sub_center=new message_filters::Subscriber<Image> (nh, "/center/image_color", 1);
                
                sync = new Synchronizer<MySyncPolicy> (MySyncPolicy(10), *image_sub_left, *image_sub_right, *image_sub_center);
                sync->registerCallback(boost::bind(&PointGreyGSCamerasSyncNodelet::imageCallback,this, _1, _2, _3));
                


                               
                

            }
            
            string frame_id_left, frame_id_right, frame_id_center, img_topic_left,img_topic_center, img_topic_right,meta_info_topic_left,meta_info_topic_right; 

            
            ros::ServiceClient clientNormTrigger;
            ros::ServiceClient clientSoftTrigRight;
            ros::ServiceClient startSoftTrigRight;


            message_filters::Subscriber<Image> *image_sub_left;
            //message_filters::Subscriber<wfov_camera_msgs::jir_capture_msg> *info_sub_left;

            message_filters::Subscriber<Image> *image_sub_right;
            //message_filters::Subscriber<wfov_camera_msgs::jir_capture_msg> *info_sub_right;

            message_filters::Subscriber<Image> *image_sub_center;
            //message_filters::Subscriber<wfov_camera_msgs::jir_capture_msg> *info_sub_center;

            typedef sync_policies::ApproximateTime<Image, Image, Image> MySyncPolicy;
            Synchronizer <MySyncPolicy> *sync;


            
            image_transport::ImageTransport *it;
            image_transport::Publisher left_pub;
            image_transport::Publisher right_pub;
            image_transport::Publisher center_pub;


    };

}
PLUGINLIB_EXPORT_CLASS(pointgrey_camera_driver::PointGreyGSCamerasSyncNodelet, nodelet::Nodelet) ;
