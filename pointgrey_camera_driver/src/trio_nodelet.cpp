/*
This code was developed by the National Robotics Engineering Center (NREC), part of the Robotics Institute at Carnegie Mellon University.
Its development was funded by DARPA under the LS3 program and submitted for public release on June 7th, 2012.
Release was granted on August, 21st 2012 with Distribution Statement "A" (Approved for Public Release, Distribution Unlimited).

This software is released under a BSD license:

Copyright (c) 2012, Carnegie Mellon University. All rights reserved.

Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:

Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.
Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in the documentation and/or other materials provided with the distribution.
Neither the name of the Carnegie Mellon University nor the names of its contributors may be used to endorse or promote products derived from this software without specific prior written permission.
THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/



/**
   @file trio_nodelet.cpp
   @author Igor Sokolovski
   @date June 2014
   @brief ROS nodelet for the Point Grey Trio Cameras

   @attention Copyright (C) 2012
   @attention National Robotics Engineering Center
   @attention Carnegie Mellon University
*/

// ROS and associated nodelet interface and PLUGINLIB declaration header
#include "ros/ros.h"
#include <pluginlib/class_list_macros.h>
#include <nodelet/nodelet.h>

#include "pointgrey_camera_driver/PointGreyCamera.h" // The actual standalone library for the PointGreys

#include <image_transport/image_transport.h> // ROS library that allows sending compressed images
#include <camera_info_manager/camera_info_manager.h> // ROS library that publishes CameraInfo topics
#include <sensor_msgs/CameraInfo.h> // ROS message header for CameraInfo
#include <std_msgs/Float64.h>

#include <wfov_camera_msgs/WFOVImage.h>
#include <wfov_camera_msgs/jir_capture_msg.h>
#include <image_exposure_msgs/ExposureSequence.h> // Message type for configuring gain and white balance.

#include <diagnostic_updater/diagnostic_updater.h> // Headers for publishing diagnostic messages.
#include <diagnostic_updater/publisher.h>

#include <boost/thread.hpp> // Needed for the nodelet to launch the reading thread.

#include <dynamic_reconfigure/server.h> // Needed for the dynamic_reconfigure gui service to run

namespace pointgrey_camera_driver
{

class PointGreyTrioCameraNodelet: public nodelet::Nodelet
{
public:
    PointGreyTrioCameraNodelet() {}

    ~PointGreyTrioCameraNodelet()
    {
        pubThread_->interrupt();
        pubThread_->join();
        cleanUp();
    }

private:
    /*!
    * \brief Function that allows reconfiguration of the camera.
    *
    * This function serves as a callback for the dynamic reconfigure service.  It simply passes the configuration object to the driver to allow the camera to reconfigure.
    * \param config  camera_library::CameraConfig object passed by reference.  Values will be changed to those the driver is currently using.
    * \param level driver_base reconfiguration level.  See driver_base/SensorLevels.h for more information.
    */

    void paramCallback ( pointgrey_camera_driver::PointGreyConfig &config, uint32_t level )
    {

        // Trio is only active in this mode (24 bits, 8 for each image)
        config.video_mode = "format7_mode3";
        config.format7_color_coding = "rgb8";
        config.format7_packet_size = 7680;
        //config.frame_rate = 16;
//	return;
        try
            {
                NODELET_DEBUG ( "Dynamic reconfigure callback with level: %d", level );
                pg_.setNewConfiguration ( config, level );
                // Store needed parameters for the metadata message
                gain_ = config.gain;
                wb_blue_ = config.white_balance_blue;
                wb_red_ = config.white_balance_red;

                // Zeros mean the full resolution was captured.
                roi_x_offset_ = 0;
                roi_y_offset_ = 0;
                roi_height_ = 0;
                roi_width_ = 0;
                do_rectify_ = false; // Set to false if the whole image is captured.
                saved_config = config;
                saved_level = level;
            }

        catch ( std::runtime_error& e )
            {
                NODELET_ERROR ( "Reconfigure Callback failed with error: %s", e.what() );
            }
    }

    /*!
    * \brief Serves as a psuedo constructor for nodelets.
    *
    * This function needs to do the MINIMUM amount of work to get the nodelet running.  Nodelets should not call blocking functions here for a significant period of time.
    */
    void onInit()
    {
        // Get nodeHandles
        ros::NodeHandle &nh = getMTNodeHandle();
        ros::NodeHandle &pnh = getMTPrivateNodeHandle();
        std::string left_cam, center_cam, right_cam;
        pnh.param<std::string> ( "left_ns", left_cam, "left" );
        ros::NodeHandle l_nh ( getMTNodeHandle(), left_cam );
        pnh.param<std::string> ( "center_ns", center_cam, "center" );
        ros::NodeHandle c_nh ( getMTNodeHandle(), center_cam );
        pnh.param<std::string> ( "right_ns", right_cam, "right" );
        ros::NodeHandle r_nh ( getMTNodeHandle(), right_cam );

        // Get a serial number through ros
        int serialParam;
        pnh.param<int> ( "serial", serialParam, 0 );
        uint32_t serial = ( uint32_t ) serialParam;

        // Get the location of our camera config yamls
        std::string l_camera_info_url, c_camera_info_url, r_camera_info_url;
        pnh.param<std::string> ( "left_camera_info_url", l_camera_info_url, "" );
        pnh.param<std::string> ( "center_camera_info_url", c_camera_info_url, "" );
        pnh.param<std::string> ( "right_camera_info_url", r_camera_info_url, "" );

        // Get the desired frame_id, set to 'camera' if not found
        pnh.param<std::string> ( "frame_id", frame_id_, "camera" );
        pnh.param<std::string> ( "second_frame_id", second_frame_id_, frame_id_ ); // Default to left frame_id per stereo API
        pnh.param<std::string> ( "third_frame_id", third_frame_id_, frame_id_ );

        // Set the timeocallCallbackut for grabbing images from the network
        double timeout;
        pnh.param ( "timeout", timeout, 1.0 );

        // Try connecting to the camera
        volatile bool connected = false;

        while ( !connected && ros::ok() )
            {
                try
                    {
                        NODELET_INFO ( "Connecting to camera serial: %u", serial );
                        pg_.setDesiredCamera ( serial );
                        NODELET_DEBUG ( "Actually connecting to camera." );
                        pg_.connect();
                        connected = true;
                        NODELET_DEBUG ( "Setting timeout to: %f.", timeout );
                        pg_.setTimeout ( timeout );
                    }

                catch ( std::runtime_error& e )
                    {
                        NODELET_ERROR ( "%s", e.what() );
                        ros::Duration ( 1.0 ).sleep(); // sleep for one second each time
                    }
            }

        // Start up the dynamic_reconfigure service, note that this needs to stick around after this function ends
        srv_ = boost::make_shared <dynamic_reconfigure::Server<pointgrey_camera_driver::PointGreyConfig> > ( pnh );
        dynamic_reconfigure::Server<pointgrey_camera_driver::PointGreyConfig>::CallbackType f =  boost::bind ( &pointgrey_camera_driver::PointGreyTrioCameraNodelet::paramCallback, this, _1, _2 );
        srv_->setCallback ( f );

        // Start the camera info manager and attempt to load any configurations
        std::stringstream cinfo_name;
        cinfo_name << serial;

        l_info_mng_.reset ( new camera_info_manager::CameraInfoManager ( l_nh, cinfo_name.str(), l_camera_info_url ) );
        c_info_mng_.reset ( new camera_info_manager::CameraInfoManager ( c_nh, cinfo_name.str(), c_camera_info_url ) );
        r_info_mng_.reset ( new camera_info_manager::CameraInfoManager ( r_nh, cinfo_name.str(), r_camera_info_url ) );

        /*if (cinfo_->validateURL(camera_info_url)){ // Load CameraInfo (if any) from the URL.  Will ROS_ERROR if bad.
          cinfo_->loadCameraInfo(camera_info_url);
        }*/
        l_info_.reset ( new sensor_msgs::CameraInfo ( l_info_mng_->getCameraInfo() ) );
        l_info_->header.frame_id = frame_id_;
        c_info_.reset ( new sensor_msgs::CameraInfo ( c_info_mng_->getCameraInfo() ) );
        c_info_->header.frame_id = second_frame_id_;
        r_info_.reset ( new sensor_msgs::CameraInfo ( r_info_mng_->getCameraInfo() ) );
        r_info_->header.frame_id = third_frame_id_;

        // Publish topics using ImageTransport through camera_info_manager (gives cool things like compression)
        l_it_.reset ( new image_transport::ImageTransport ( l_nh ) );
        l_pub_ = l_it_->advertiseCamera ( "image_raw", 5 );
        c_it_.reset ( new image_transport::ImageTransport ( c_nh ) );
        c_pub_ = c_it_->advertiseCamera ( "image_raw", 5 );
        r_it_.reset ( new image_transport::ImageTransport ( r_nh ) );
        r_pub_ = r_it_->advertiseCamera ( "image_raw", 5 );

        // Set up diagnostics
        updater_.setHardwareID ( "pointgrey_camera " + serial );

        ///< @todo Move this to diagnostics
        temp_pub_ = nh.advertise<std_msgs::Float64> ( "temp", 5 );

 std::stringstream image_info_topic;
    image_info_topic<<frame_id_;
    image_info_topic<<"/imageinfo";
    //std::string str=image_info_topic.str();
    //NODELET_ERROR("%s",str.c_str());
    image_data_publisher=nh.advertise<wfov_camera_msgs::jir_capture_msg> (image_info_topic.str(),1000);

        // Subscribe to gain and white balance changes
        sub_ = nh.subscribe ( "image_exposure_sequence", 10, &pointgrey_camera_driver::PointGreyTrioCameraNodelet::gainWBCallback, this );

        volatile bool started = false;

        while ( !started )
            {
                try
                    {
                        NODELET_DEBUG ( "Starting camera capture." );
                        pg_.start();
                        started = true;
                        // Start the thread to loop through and publish messages
                        pubThread_ = boost::shared_ptr< boost::thread > ( new boost::thread ( boost::bind ( &pointgrey_camera_driver::PointGreyTrioCameraNodelet::devicePoll, this ) ) );
                    }

                catch ( std::runtime_error& e )
                    {
                        NODELET_ERROR ( "%s", e.what() );
                        ros::Duration ( 1.0 ).sleep(); // sleep for one second each time
                    }
            }
    }

    /*!
    * \brief Cleans up the memory and disconnects the camera.
    *
    * This function is called from the deconstructor since pg_.stop() and pg_.disconnect() could throw exceptions.
    */
    void cleanUp()
    {
        try
            {
                NODELET_DEBUG ( "Stopping camera capture." );
                pg_.stop();
                NODELET_DEBUG ( "Disconnecting from camera." );
                pg_.disconnect();
            }

        catch ( std::runtime_error& e )
            {
                NODELET_ERROR ( "%s", e.what() );
            }
    }

    /*!
    * \brief Function for the boost::thread to grabImages and publish them.
    *
    * This function continues until the thread is interupted.  Responsible for getting sensor_msgs::Image and publishing them.
    */
    void devicePoll()
    {
        while ( !boost::this_thread::interruption_requested() ) // Block until we need to stop this thread.
            {
                try
                    {
                        //TODO check, maybe it is necessary to paste it into the init function
                        l_image_.reset ( new sensor_msgs::Image() );
                        c_image_.reset ( new sensor_msgs::Image() );
                        r_image_.reset ( new sensor_msgs::Image() );

			FlyCapture2::ImageMetadata metadata;
        		FlyCapture2::TimeStamp timestamp;
                        pg_.grabTrioImage ( *l_image_, *c_image_, *r_image_ ,metadata,timestamp);

                        ros::Time time = ros::Time::now();
                        l_image_->header.stamp = time;
                        c_image_->header.stamp = time;
                        r_image_->header.stamp = time;
                        l_image_->header.frame_id = frame_id_;
                        c_image_->header.frame_id = second_frame_id_;
                        r_image_->header.frame_id = third_frame_id_;

                        l_info_->header.stamp = time;
                        c_info_->header.stamp = time;
                        r_info_->header.stamp = time;
                        ///////////////////TODO delete this stupid code
                        fillInfo ( l_info_ );
                        fillInfo ( c_info_ );
                        fillInfo ( r_info_ );

                        l_pub_.publish ( l_image_, l_info_ );
                        c_pub_.publish ( c_image_, c_info_ );
                        r_pub_.publish ( r_image_, r_info_ );

//image metadata and timestamp
        wfov_camera_msgs::jir_capture_msg image_capture_data;
        image_capture_data.header=r_info_->header;

        image_capture_data.gain=metadata.embeddedGain;
        image_capture_data.shutter=metadata.embeddedShutter;
        image_capture_data.brightness=metadata.embeddedBrightness;
        image_capture_data.exposure=metadata.embeddedExposure;
        image_capture_data.whiteBalance=metadata.embeddedWhiteBalance;
        image_capture_data.frameCounter=metadata.embeddedFrameCounter;
        image_capture_data.embeddedTimeStamp=metadata.embeddedTimeStamp;

        image_capture_data.seconds=timestamp.seconds;
        image_capture_data.microSeconds=timestamp.microSeconds;
        image_capture_data.cycleSeconds=timestamp.cycleSeconds;
        image_capture_data.cycleCount=timestamp.cycleCount;
        image_capture_data.cycleOffset=timestamp.cycleOffset;

	image_data_publisher.publish(image_capture_data);

                        //std_msgs::Float64 temp;
                        //temp.data = pg_.getCameraTemperature();
                        //temp_pub_.publish(temp);
                    }

                catch ( CameraTimeoutException& e )
                    {
                        NODELET_WARN ( "%s", e.what() );
                    }

                catch ( std::runtime_error& e )
                    {
                        NODELET_ERROR ( "%s", e.what() );

                        try
                            {
                                // Something terrible has happened, so let's just disconnect and reconnect to see if we can recover.
                                ROS_INFO ( "Try to reconnect" );
                                pg_.disconnect();
                                ros::Duration ( 1.0 ).sleep(); // sleep for one second each time
                                //Call rescan camera list.
                                //pg_.rescanBus();
                                pg_.connect();
                                //pg_.restoreConfiguration ( saved_config, saved_level );
                                pg_.start();
                            }

                        catch ( std::runtime_error& e2 )
                            {
                                NODELET_ERROR ( "%s", e2.what() );
                            }
                    }

                // Update diagnostics
                //updater_.update();
            }
    }

    void gainWBCallback ( const image_exposure_msgs::ExposureSequence &msg )
    {
        try
            {
                NODELET_DEBUG ( "Gain callback:  Setting gain to %f and white balances to %u, %u", msg.gain, msg.white_balance_blue, msg.white_balance_red );
                gain_ = msg.gain;
                pg_.setGain ( gain_ );
                wb_blue_ = msg.white_balance_blue;
                wb_red_ = msg.white_balance_red;
                pg_.setBRWhiteBalance (false, wb_blue_, wb_red_ );
            }

        catch ( std::runtime_error& e )
            {
                NODELET_ERROR ( "gainWBCallback failed with error: %s", e.what() );
            }
    }

    void fillInfo ( sensor_msgs::CameraInfoPtr  c_info )
    {
        c_info->binning_x = binning_x_;
        c_info->binning_y = binning_y_;
        c_info->roi.x_offset = roi_x_offset_;
        c_info->roi.y_offset = roi_y_offset_;
        c_info->roi.height = roi_height_;
        c_info->roi.width = roi_width_;
        c_info->roi.do_rectify = do_rectify_;
    }

    pointgrey_camera_driver::PointGreyConfig saved_config;
    uint32_t saved_level;
    boost::shared_ptr<dynamic_reconfigure::Server<pointgrey_camera_driver::PointGreyConfig> > srv_; ///< Needed to initialize and keep the dynamic_reconfigure::Server in scope.

    boost::shared_ptr<image_transport::ImageTransport> l_it_, c_it_, r_it_; ///< Needed to initialize and keep the ImageTransport in scope.
    boost::shared_ptr<camera_info_manager::CameraInfoManager> l_info_mng_, r_info_mng_, c_info_mng_; ///< Needed to initialize and keep the CameraInfoManager in scope.
    image_transport::CameraPublisher l_pub_, c_pub_, r_pub_; ///< CameraInfoManager ROS publisher
    sensor_msgs::ImagePtr l_image_, c_image_, r_image_;
    sensor_msgs::CameraInfoPtr l_info_, c_info_, r_info_; ///< Camera Info message.

ros::Publisher image_data_publisher;

    // For trio cameras
    std::string frame_id_; ///< Frame id for the camera messages, defaults to 'camera'
    std::string second_frame_id_; ///< Frame id used for the second camera.
    std::string third_frame_id_; ///< Frame id used for the third camera.

    ros::Publisher temp_pub_; ///< Publisher for current camera temperature @todo Put this in diagnostics instead.
    ros::Subscriber sub_; ///< Subscriber for gain and white balance changes.

    diagnostic_updater::Updater updater_; ///< Handles publishing diagnostics messages.
    double min_freq_;
    double max_freq_;
    PointGreyCamera pg_; ///< Instance of the PointGreyCamera library, used to interface with the hardware.

    boost::shared_ptr<boost::thread> pubThread_; ///< The thread that reads and publishes the images.

    double gain_;
    uint16_t wb_blue_;
    uint16_t wb_red_;

    // Parameters for cameraInfo
    size_t binning_x_; ///< Camera Info pixel binning along the image x axis.
    size_t binning_y_; ///< Camera Info pixel binning along the image y axis.
    size_t roi_x_offset_; ///< Camera Info ROI x offset
    size_t roi_y_offset_; ///< Camera Info ROI y offset
    size_t roi_height_; ///< Camera Info ROI height
    size_t roi_width_; ///< Camera Info ROI width
    bool do_rectify_; ///< Whether or not to rectify as if part of an image.  Set to false if whole image, and true if in ROI mode.
};

PLUGINLIB_DECLARE_CLASS ( pointgrey_camera_driver, PointGreyTrioCameraNodelet, pointgrey_camera_driver::PointGreyTrioCameraNodelet, nodelet::Nodelet ); // Needed for Nodelet declaration
}
